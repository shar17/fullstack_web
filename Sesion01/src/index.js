//console.log("Hola mundo desde Javascript lunes");
// operadores
const n1 = document.querySelector('#primer_numero')
const n2 = document.querySelector('#segundo_numero')
// operaciones
const sum = document.querySelector("#sum_button") 
const dif = document.querySelector("#dif_button") 
// resultado
const resultNode = document.querySelector("#resultado")

sum.addEventListener('click', function(){
    //transformar en entero el contenido de cada numero
    const n1Int = parseInt(n1.value)
    const n2Int = parseInt(n2.value)
    //operacion
    const result = n1Int + n2Int
    console.log(result);
    //creamos nodo <span>  y lo combinamos con el resultado
    const texNode = document.createTextNode(result)
    const spanElement = document.createElement('span')
    spanElement.appendChild(texNode)

    //eliminamos resultado previo
    if (resultNode.childNodes.length>3){
        resultNode.removeChild(resultNode.childNodes[3])
    }

    resultNode.appendChild(spanElement) 
})

dif.addEventListener('click', function(){
    //transformar en entero el contenido de cada numero
    const n1Int = parseInt(n1.value)
    const n2Int = parseInt(n2.value)
    //operacion
    const result = n1Int - n2Int
    console.log(result);
    //creamos nodo <span>  y lo combinamos con el resultado
    const texNode = document.createTextNode(result)
    const spanElement = document.createElement('span')
    spanElement.appendChild(texNode)

    //eliminamos resultado previo
    if (resultNode.childNodes.length>3){
        resultNode.removeChild(resultNode.childNodes[3])
    }

    resultNode.appendChild(spanElement) 
})

